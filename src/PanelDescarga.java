import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class PanelDescarga extends SwingWorker<Void, Integer> implements ActionListener{
    private JPanel panel;
    private JProgressBar progressbar;
    private JButton btcerrar;
    private JLabel lbProgreso;
    private JLabel lbNombre;
    private JButton btpausa;
    private JButton btplay;
    private boolean seguir;
    private Ventana ventana;
    private String extension;
    private String path;
    private String nombre;
    private String urlDescarga, urlDirectorio;

    public JPanel getPanel() {
        return panel;
    }

    public PanelDescarga(Ventana ventana, String urlDescarga, String urlDirectorio) {
        this.ventana = ventana;

        path   = urlDescarga;
        nombre = path.substring(path.lastIndexOf('/') + 1);
        extension = path.substring(path.lastIndexOf('.'));
        lbNombre.setText(nombre);

        panel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));
        btcerrar.addActionListener(this);
        btpausa.addActionListener(this);
        btplay.addActionListener(this);
        this.urlDescarga = urlDescarga;
        this.urlDirectorio = urlDirectorio;
        seguir = true;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton boton = (JButton) e.getSource();
        if (boton.equals(btcerrar)){
            eliminarDescarga();
            if (seguir) {
                seguir = false;
                ventana.descargando--;
                ventana.lbEstado.setText("Descargando: " + ventana.descargando + "/" + ventana.numeroDescargas);
            }
        }else if (boton.equals(btpausa)){
            if (seguir) {
                seguir = false;
                ventana.descargando--;
                ventana.lbEstado.setText("Descargando: " + ventana.descargando + "/" + ventana.numeroDescargas);
                ventana.logger.info("Descarga " + nombre + " cancelada");
            }
        }else if (boton.equals(btplay)){
            ventana.reanudar(this);
            ventana.logger.info("Descarga " + nombre + " reiniciada");
        }
    }

    public void eliminarDescarga(){
        if (JOptionPane.showConfirmDialog(ventana, "¿Quieres eliminar la descarga?", "Eliminar", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
            ventana.getPaneles().remove(panel);
            ventana.eliminar(this);
            ventana.getListaPaneles().updateUI();
            if (JOptionPane.showConfirmDialog(ventana, "¿Quieres eliminar los archivos descargados?", "Eliminar", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                File file = new File(urlDirectorio+extension);
                if (file.delete()){
                    JOptionPane.showMessageDialog(ventana, "Archivo borrado correctamente");
                    ventana.logger.info("Descarga de "+ nombre + " eliminada");
                }else {
                    JOptionPane.showMessageDialog(ventana, "El Archivo está corrupto, en uso o no existe");
                    ventana.logger.error("Error al eliminar los archivos");
                }
            }
        }
    }

    public String getExtension() {
        return extension;
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    protected Void doInBackground() throws Exception {
        URL url = new URL(urlDescarga);

        URLConnection conexion = url.openConnection();
        int tamanoFichero = conexion.getContentLength();

        InputStream is = url.openStream();
        FileOutputStream fos = new FileOutputStream(urlDirectorio+extension);
        byte[] bytes = new byte[2048];
        int longitud = 0;
        int progresoDescarga = 0;

        while ((longitud = is.read(bytes)) != -1 && seguir) {
            fos.write(bytes, 0, longitud);

            progresoDescarga += longitud;
            setProgress(progresoDescarga * 100 / tamanoFichero);
            progressbar.setValue(getProgress());
            lbProgreso.setText(String.valueOf(getProgress())+"%" + "  ||  " + progresoDescarga/1000 +"/"+ tamanoFichero/1000+"kb");
        }

        is.close();
        fos.close();
        if (seguir) {
            ventana.descargando--;
            ventana.lbEstado.setText("Descargando: " + ventana.descargando + "/" + ventana.numeroDescargas);
            ventana.logger.info("Descarga de " +nombre+ " completada");
        }
        seguir = false;

        setProgress(100);

        return null;
    }

    public String getUrlDescarga() {
        return urlDescarga;
    }

    public String getUrlDirectorio() {
        return urlDirectorio;
    }
}
