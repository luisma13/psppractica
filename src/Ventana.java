import org.apache.commons.validator.routines.UrlValidator;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

public class Ventana extends JFrame implements ActionListener, KeyListener{
    private JTextField rutaFichero;
    private JPanel panel;
    private JButton btanadir;
    private JPanel listaPaneles;
    private JScrollPane scrollPane;
    private JButton btLimpiar;
    private ArrayList<JPanel> paneles;

    public JLabel lbEstado;
    public Logger logger;

    private File archivoNombradoDescarga;
    private Timer timer;
    private int tiempoTotal;
    private String rutaProgramada;

    private JMenu archivo;
    public int numeroDescargas;
    public int descargando;
    private File archivoDescarga;

    public static void main(String[] args) {
        new Ventana();
    }

    public ArrayList<JPanel> getPaneles() {
        return paneles;
    }

    public JPanel getListaPaneles() {
        return listaPaneles;
    }

    public Ventana(){
        splashScreen();

        org.apache.log4j.BasicConfigurator.configure();
        logger = Logger.getLogger("log_LuismaDownloader.log");

        try {
            logger.addAppender(new FileAppender(new PatternLayout(), "log_LuismaDownloader.log", true));
        } catch (IOException e) {
            e.printStackTrace();
        }

        numeroDescargas = 5;
        descargando = 0;
        paneles = new ArrayList<JPanel>();
        setContentPane(panel);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("DownLoader v1.0 Luisma");
        pack();
        setVisible(true);
        setLocationRelativeTo(null);
        setResizable(false);
        lbEstado.setText("Descargando: " + descargando + "/" + numeroDescargas);
        JMenuBar barra;
        barra = new JMenuBar();
        archivo = new JMenu("Archivo");
        barra.add(archivo);
        archivo.add("Seleccionar ruta de las descargas");
        archivo.add("Número máximo de descargas simultáneas");
        archivo.add("Programar descarga");
        archivo.add("Mostrar log");
        archivo.getItem(0).addActionListener(this);
        archivo.getItem(1).addActionListener(this);
        archivo.getItem(2).addActionListener(this);
        archivo.getItem(3).addActionListener(this);
        setJMenuBar(barra);
        init();
    }

    public void splashScreen(){
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        JWindow window = new JWindow();
        window.getContentPane().setLayout(new BorderLayout());
        JProgressBar barra = new JProgressBar();
        window.getContentPane().add(barra, BorderLayout.SOUTH);
        window.getContentPane().add(
                new JLabel("", new ImageIcon(getClass().getResource("splash.jpg")), SwingConstants.CENTER), BorderLayout.CENTER);
        window.setBounds((int)toolkit.getScreenSize().getWidth()/2 - 150,
                (int) toolkit.getScreenSize().getHeight()/2 - 100,
                300, 200);
        window.setVisible(true);
        try {
            int cuenta = 0;
            while (cuenta < 5) {
                cuenta++;
                barra.setValue(cuenta*20);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        window.setVisible(false);
        window.dispose();
    }

    public void init(){
        btanadir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                anadir();
                btLimpiar.setEnabled(false);
            }
        });
        btLimpiar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                rutaFichero.setText("");
                btLimpiar.setEnabled(false);
            }
        });
        listaPaneles.setLayout(new BoxLayout(listaPaneles, BoxLayout.Y_AXIS));
        rutaFichero.addKeyListener(this);
        btanadir.setEnabled(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JMenuItem item = (JMenuItem) e.getSource();
        if (item == archivo.getItem(0)){

            rutaGuardado();

        }else if (item == archivo.getItem(1)) {

            maximoDescargas();

        }else if (item == archivo.getItem(2)){

            programar();

        }else if (item == archivo.getItem(3)){
            File file = new File("log_LuismaDownloader.log");
            try {
                Desktop.getDesktop().open(file);
                System.out.println(file.getAbsolutePath());
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    public void rutaGuardado(){
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(archivoDescarga);
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if (fileChooser.showOpenDialog(this) == JFileChooser.CANCEL_OPTION)
            return;
        archivoDescarga = fileChooser.getSelectedFile();
    }

    public void maximoDescargas(){
        try {
            String opcion = JOptionPane.showInputDialog(this, "Máximo de descargas simultaneas", numeroDescargas);
            if (!opcion.equals("")) {
                numeroDescargas = Integer.parseInt(opcion);
                if (numeroDescargas == 0) {
                    JOptionPane.showMessageDialog(this, "El campo no puede ser 0. Por defecto serán 5");
                    numeroDescargas = 5;
                }
            } else {
                JOptionPane.showMessageDialog(this, "El campo no puede nulo. Por defecto serán 5");
                numeroDescargas = 5;
            }
        } catch (NumberFormatException nfe) {
            numeroDescargas = 5;
            JOptionPane.showMessageDialog(this, "Error al introducir el numero. Por defecto serán 5");
        }
        lbEstado.setText("Descargando: " + descargando + "/" + numeroDescargas);
    }

    public void programar(){
        String horasString = JOptionPane.showInputDialog(this, "Introduce horas");
        String minutosString = JOptionPane.showInputDialog(this, "Introduce minutos");
        rutaProgramada = JOptionPane.showInputDialog(this, "Introduce la url");

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(archivoDescarga);
        if (fileChooser.showOpenDialog(this) == JFileChooser.CANCEL_OPTION) {
            return;
        }
        archivoNombradoDescarga = fileChooser.getSelectedFile();

        int horas, minutos;

        if (horasString != null)
            horas = Integer.parseInt(horasString);
        else
            horas = 0;

        if (minutosString != null)
            minutos = Integer.parseInt(minutosString);
        else
            minutos = 0;

        tiempoTotal = (horas*3600) + (minutos*60);

        timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                programacion();
            }
        });
        timer.start();
    }

    public void programacion(){
        if (tiempoTotal > 0)
            tiempoTotal--;
        if (tiempoTotal == 0){
            PanelDescarga panelDescarga = new PanelDescarga(this, rutaProgramada, archivoNombradoDescarga.getAbsolutePath());
            panelDescarga.execute();
            paneles.add(panelDescarga.getPanel());
            refrescarPaneles();
            descargando++;
            lbEstado.setText("Descargando: " + descargando + "/" + numeroDescargas);
            timer.stop();
            logger.info("Descarga programada de " + panelDescarga.getNombre() + panelDescarga.getExtension() + " comenzada");
        }
    }

    public void anadir(){
        if (new UrlValidator().isValid(rutaFichero.getText())) {

            if (descargando < numeroDescargas) {
                PanelDescarga panelDescarga = null;

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(archivoDescarga);
                if (fileChooser.showOpenDialog(this) == JFileChooser.CANCEL_OPTION) {
                    return;
                }
                archivoNombradoDescarga = fileChooser.getSelectedFile();
                panelDescarga = new PanelDescarga(this, rutaFichero.getText(), archivoNombradoDescarga.getAbsolutePath());
                panelDescarga.execute();
                descargando++;

                paneles.add(panelDescarga.getPanel());
                listaPaneles.add(panelDescarga.getPanel());
                listaPaneles.updateUI();
                rutaFichero.setText("");
                btanadir.setEnabled(false);
                lbEstado.setText("Descargando: " + descargando + "/" + numeroDescargas);
            } else {
                JOptionPane.showMessageDialog(this, "Alcanzado el número máximo de descargas");//
            }
        }else{
            JOptionPane.showMessageDialog(this, "La ruta no es válida");
        }
    }

    public void reanudar(PanelDescarga panel){
        PanelDescarga nuevoPanel = new PanelDescarga(this, panel.getUrlDescarga(), panel.getUrlDirectorio());
        paneles.set(paneles.indexOf(panel.getPanel()), nuevoPanel.getPanel());
        nuevoPanel.execute();
        refrescarPaneles();
        descargando++;
        lbEstado.setText("Descargando: " + descargando + "/" + numeroDescargas);
    }

    public void refrescarPaneles(){
        listaPaneles.removeAll();
        for (JPanel panel : paneles){
            listaPaneles.add(panel);
        }
        listaPaneles.updateUI();
    }

    public void eliminar(PanelDescarga panelDescarga){
        paneles.remove(panelDescarga.getPanel());
        listaPaneles.remove(panelDescarga.getPanel());
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (rutaFichero.getText().equals("")){
            btanadir.setEnabled(false);
            btLimpiar.setEnabled(false);
        }else {
            btanadir.setEnabled(true);
            btLimpiar.setEnabled(true);
        }
    }
}
